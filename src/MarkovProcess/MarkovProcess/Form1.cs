﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkovProcess
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        DataTable GetSearchResults(int count)
        {
            DataTable table = new DataTable();
            return table;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var count = Convert.ToInt32(numericCount.Text);

            SetDGstates(count);
            SetDGProbabilities(count);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var count = DGstates.ColumnCount;

            int info;
            var probabilities = new double[count];
            var rep = new alglib.densesolverreport();
            alglib.rmatrixsolve(GetDataFromDG(count), count, GetRightPart(count), out info, out rep, out probabilities);

            SetDGProbabilities(probabilities);
        }

        private double[,] GetDataFromDG(int count)
        {
            var matrix = new double[DGstates.RowCount, DGstates.ColumnCount];

            for (int r = 0; r < count; r++)
            {
                for (int c = 0; c < count; c++)
                {
                    matrix[r, c] = 0;
                }
            }

            for (int s = 0; s < count-1; s++)
            {
                for (int r = 0; r < count; r++)
                {
                    matrix[s, s] += Convert.ToDouble(DGstates[s, r].Value);
                }
                for (int c = 0; c < count; c++)
                {
                    if (c != s)
                    {
                        matrix[s, c] -= Convert.ToDouble(DGstates[c, s].Value);
                    }
                }
            }

            for (int i = 0; i < count; i++)
            {
                matrix[count-1, i] = 1;
            }

            return matrix;
        }

        private double[] GetRightPart(int count)
        {
            var rightpart = new double[count];
            for (int i = 0; i < count-1; i++)
            {
                rightpart[i] = 0;
            }
            rightpart[count - 1] = 1;
            return rightpart;
        }

        private void SetDGstates(int count)
        {
            DGstates.ColumnCount = count;
            for (int i = 0; i < count; i++)
            {
                DGstates.Columns[i].Name = "s[" + i + "]";
            }

            DGstates.RowCount = count;
            for (int i = 0; i < count; i++)
            {
                DGstates.Rows[i].HeaderCell.Value = "s[" + i + "]";
            }
        }

        private void SetDGProbabilities(int count)
        {
            DGprobabilities.ColumnCount = count;
            for (int i = 0; i < count; i++)
            {
                DGprobabilities.Columns[i].Name = "p[" + i + "]";
            }
            DGprobabilities.RowCount = 1;
        }

        private void SetDGProbabilities(double[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                DGprobabilities[i,0].Value = Math.Round(array[i],2);
            }
        }

        

    }

    
}
